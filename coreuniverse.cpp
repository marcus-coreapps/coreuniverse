/*
	*
	* This file is a part of CoreUniverse.
	* Shows brief info of all C Suite apps.
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QAbstractButton>
#include <QPushButton>
#include <QFile>
#include <QScrollBar>
#include <QMessageBox>
#include <QScroller>
#include <QDesktopServices>
#include <QProcess>
#include <QTextStream>
#include <QToolButton>

#include <cprime/themefunc.h>

#include "settings.h"

#include "coreuniverse.h"
#include "ui_coreuniverse.h"

coreuniverse::coreuniverse(QWidget *parent): QWidget(parent)
	, ui(new Ui::coreuniverse)
	, smi(new settings)
{
	ui->setupUi(this);

	QPalette pltt(palette());
	pltt.setColor(QPalette::Base, Qt::transparent);
	setPalette(pltt);

	loadSettings();
	startSetup();
    setupIcons();

	on_about_clicked();
}

coreuniverse::~coreuniverse()
{
	delete smi;
	delete ui;
}

/**
 * @brief Setup ui elements
 */
void coreuniverse::startSetup()
{
	// all toolbuttons icon size in sideView
	for (QToolButton *b : ui->sideView->findChildren<QToolButton *>()) {
		if (b) {
			b->setIconSize(toolsIconSize);
		}
	}

	ui->suiteNameVersion->setText("C Suite v" + QString(VERSION_TEXT));
	ui->menu->setVisible(0);
	ui->appTitle->setAttribute(Qt::WA_TransparentForMouseEvents);
	ui->appTitle->setFocusPolicy(Qt::NoFocus);
	this->resize(800, 500);

    if (uiMode == 2) {
        // setup mobile UI
        this->setWindowState(Qt::WindowMaximized);

        QScroller::grabGesture(ui->pgother, QScroller::LeftMouseButtonGesture);
        ui->menu->setIconSize(toolsIconSize);

        connect(ui->menu, &QToolButton::clicked, this, &coreuniverse::showSideView);
        connect(ui->appTitle, &QToolButton::clicked, this, &coreuniverse::showSideView);

        ui->sideView->setVisible(0);
        ui->menu->setVisible(1);
    } else {
        // setup desktop or tablet UI

        if(windowMaximized){
            this->setWindowState(Qt::WindowMaximized);
            qDebug() << "window is maximized";
        } else{
            this->resize(windowSize);
        }
    }
}

void coreuniverse::setupIcons()
{
    ui->about->setIcon(CPrime::ThemeFunc::themeIcon( "dialog-information-symbolic", "dialog-information", "dialog-information" ));
    ui->credits->setIcon(CPrime::ThemeFunc::themeIcon( "bookmark-new-symbolic", "bookmark-new-symbolic", "bookmark-new" ));
    ui->releaseNotes->setIcon(CPrime::ThemeFunc::themeIcon( "document-send-symbolic", "document-send-symbolic", "document-send" ));
    ui->recommendation->setIcon(CPrime::ThemeFunc::themeIcon("help-contents-symbolic", "help-contents", "help-contents" ));
    ui->copyright->setIcon(CPrime::ThemeFunc::themeIcon("dialog-warning-symbolic", "dialog-warning", "dialog-warning" ));
    ui->changelog->setIcon(CPrime::ThemeFunc::themeIcon( "document-open-symbolic", "quickopen-file", "document-open-symbolic" ));
    ui->cSuite->setIcon(CPrime::ThemeFunc::themeIcon( "applications-utilities-symbolic", "applications-utilities", "applications-utilities" ));
    ui->menu->setIcon(CPrime::ThemeFunc::themeIcon( "open-menu-symbolic", "application-menu", "open-menu" ));
}

/**
 * @brief Loads application settings
 */
void coreuniverse::loadSettings()
{
    // get CSuite's settings
    uiMode = smi->getValue("CoreApps", "UIMode");
    toolsIconSize = smi->getValue("CoreApps", "ToolsIconSize");

    // get app's settings
    windowSize = smi->getValue("CoreUniverse", "WindowSize");
    windowMaximized = smi->getValue("CoreUniverse", "WindowMaximized");

    // Sideview is visible in default
    sideView = 1;
}

void coreuniverse::pageClick(QToolButton *btn, int i, const QString &title)
{
	// first do this to get all the space for contents
	if (uiMode == 2) {
		ui->sideView->setVisible(0);
	}

	// all button checked false
	for (QToolButton *b : ui->sideView->findChildren<QToolButton *>()) {
		b->setChecked(false);
	}

	btn->setChecked(true);
	ui->selectedsection->setTextFormat(Qt::MarkdownText);
	ui->selectedsection->setText("## " + title);
	this->setWindowTitle(title + " - CoreUniverse");
	ui->pages->setCurrentIndex(i);

	ui->scrollArea->verticalScrollBar()->setSliderPosition(0);
}

void coreuniverse::showSideView()
{
	if (uiMode == 2) {
		if (ui->sideView->isVisible()) {
			ui->sideView->setVisible(0);
		} else {
			ui->sideView->setVisible(1);
		}
	}
}
void coreuniverse::on_releaseNotes_clicked()
{
	pageClick(ui->releaseNotes, 1, tr("Release Notes"));
	QFile file(":/docs/ReleaseNotes");
	file.open(QIODevice::ReadOnly | QIODevice::Text);
	QTextStream out(&file);
	ui->pgother->setText(out.readAll());
	file.close();
}

void coreuniverse::on_about_clicked()
{
	pageClick(ui->about, 0, tr("About"));
}

void coreuniverse::on_credits_clicked()
{
	pageClick(ui->credits, 1, tr("Credits"));
	QString str = QString("## Developers\n"
						  "* Abrar (@s96abrar:matrix.org)\n"
						  "* Marcus (@marcusbritanicus:matrix.org)\n"
                          "* Shaber (@rahmanshaber:matrix.org)\n\n"
						  "\n"
						  "## Package Maintainers and Testers\n"
                          "* Strit (@strit:matrix.org)\n"
                          "* xgqt (@xgqt:matrix.org)\n");
	ui->pgother->clear();
	ui->pgother->setMarkdown(str);
}

void coreuniverse::on_recommendation_clicked()
{
	pageClick(ui->recommendation, 1, tr("Recommendation"));
	QString str = QString("A few recommendations, tweaks, and tips & tricks for best experience with CoreApps.\n\n"
						  "* Use Qt5ct to change theme and icons and use Fusion as style.\n"
						  "* Use Kvantum to customize the themes.\n"
						  "* If you see fonts are not right, make a font.config using qt5ct.\n"
						  "* Please delete the coreapps.config file from home folder if you saw any ui or settings issues.\n"
						  "* Checkout other Qt apps from Manjaro's Qt app list (https://wiki.manjaro.org/index.php?title=List_of_Qt_Applications).\n"
						  "* Use Breeze Icon Theme so there won't be any missing icon in any apps.\n");
	ui->pgother->clear();
	ui->pgother->setMarkdown(str);
}

void coreuniverse::on_copyright_clicked()
{
	pageClick(ui->copyright, 1, tr("Copyright"));
	QFile file(":/docs/License");
	file.open(QIODevice::ReadOnly | QIODevice::Text);
	QTextStream out(&file);
	QString str = QString("The different components are copyrighted by their respective authors.\n\n"
						  "\n"
						  "The 19 Apps packages including this one are distributed under the terms of the "
						  "GNU General Public License as published by the Free Software Foundation; either "
						  "version 3 or later of the License, or (at your option) any later version.\n"
						  "\n"
						  "\n"
						 );
	ui->pgother->clear();
	ui->pgother->setText(str + out.readAll());
	file.close();
}

void coreuniverse::on_changelog_clicked()
{
	pageClick(ui->changelog, 1, tr("ChangeLog"));
	QFile file(":/docs/ChangeLog");
	file.open(QIODevice::ReadOnly | QIODevice::Text);
	QTextStream out(&file);
	ui->pgother->clear();
	ui->pgother->setText(out.readAll());
	file.close();
}

void coreuniverse::on_cSuite_clicked()
{
	pageClick(ui->cSuite, 1, tr("App Suite"));
	QFile file(":/docs/about.html");
	file.open(QIODevice::ReadOnly | QIODevice::Text);
	QTextStream out(&file);
	ui->pgother->clear();
	ui->pgother->setHtml(out.readAll());
	file.close();
}

void coreuniverse::on_updateBtn_clicked()
{
	QProcess download;
	download.start("wget", QStringList() << "https://gitlab.com/cubocore/coreapps-edge/raw/master/LATEST" << "-O" << "/tmp/coreapps-LATEST");
	download.waitForFinished();

	QFile upstream("/tmp/coreapps-LATEST");
	upstream.open(QFile::ReadOnly);
	QString upstreamVer = QString::fromLocal8Bit(upstream.readAll()).trimmed();
	upstream.close();

	QFile local(":/docs/LATEST");
	local.open(QFile::ReadOnly);
	QString localVer = QString::fromLocal8Bit(local.readAll()).trimmed();
	local.close();

	if (upstreamVer.isEmpty()) {
		QMessageBox::information(
			this,
			"C Suite - Unable to connect",
			"There seems to be a problem connecting to https://gitlab.com to check for "
			"updates. Please ensure you have a working internet connection and try again."
		);
	}

	else if (upstreamVer.compare(localVer) > 0) {
		QMessageBox msgBox;
		msgBox.setWindowTitle("C Suite - Update Available");
		msgBox.setText(tr("There is an update available upstream. Please download and install the latest update for the best experience with C Suite.\n"
						  "Click the 'Get Update' button to install it if your OS don't not have it."));

		QAbstractButton *pButtonYes = msgBox.addButton(tr("Get Update"), QMessageBox::YesRole);
		msgBox.addButton(tr("Nope"), QMessageBox::NoRole);
		msgBox.exec();

		if (msgBox.clickedButton() == pButtonYes) {
			QString link = "https://gitlab.com/cubocore/coreapps-edge/";
			QDesktopServices::openUrl(QUrl(link));
		}
	}

	else {
		QMessageBox::information(
			this,
			"CuboCore - Latest",
			"Hooray! You have the latest version available. Should you have any doubts, problem or suggestions feel free to open a bug report "
			"at https://gitlab.com/groups/cubocore/coreapps/-/issues"
		);
	}
}

void coreuniverse::closeEvent(QCloseEvent *cEvent)
{
    qDebug()<< "save window stats"<< this->size() << this->isMaximized();

    smi->setValue("CoreUniverse", "WindowSize", this->size());
    smi->setValue("CoreUniverse", "WindowMaximized", this->isMaximized());
}
