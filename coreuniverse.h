/*
    *
    * This file is a part of CoreUniverse.
    * Shows brief info of all C Suite apps.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QWidget>

class QToolButton;
class settings;

namespace Ui {
    class coreuniverse;
}

class coreuniverse : public QWidget {
    Q_OBJECT

public:
    explicit coreuniverse(QWidget *parent = nullptr);
    ~coreuniverse();

protected:
    void closeEvent(QCloseEvent *cEvent) override;

private slots:
    void on_releaseNotes_clicked();
    void on_about_clicked();
    void on_recommendation_clicked();
    void on_copyright_clicked();
    void on_credits_clicked();
    void on_changelog_clicked();
    void on_cSuite_clicked();
    void on_updateBtn_clicked();
    void showSideView();

private:
    Ui::coreuniverse *ui;
    bool windowMaximized, sideView;
    int uiMode;
    QSize windowSize, toolsIconSize;
    settings *smi;

	void pageClick(QToolButton *btn, int i, const QString &title);
    void startSetup();
    void loadSettings();
    void setupIcons();
};
